import java.io.File
import java.util.*


val scanner: Scanner = Scanner(System.`in`).useLocale(Locale.UK)

fun main(){
    val data = File("src/main/kotlin/data").readText()
    do {
        menu()
        // el NextLine a toInt es por el error aquel
        val instruction = scanner.nextLine().toInt()
        when (instruction){
            1 ->{
                println("Mostrant totes les lectures")
                println(data)
            }
            2 ->{
                println("Mostrant la quantitat de muntanyes per sobre de 3500")
                println(quantitatRegistresPerSobre3500(data))
            }
            3 ->{
                println("Mostrant alçada mitjana de totes les muntanyes")
                println("${alçadaMitjana(data)}m")
            }
            4 ->{
                println("Mostrant alçada mínima i màxima")
                println(alçadaMaxAndMin(data))
            }
            5 ->{
                do {
                    subMenu()
                    val subInstruction = scanner.nextLine().toInt()
                    when (subInstruction) {
                        1 -> {
                            val numDeLectures = filterOfMountainsByMeters(data, "extrem", 0, 0)
                            println("Muntanyes de més de 8000m")
                            for (lectura in numDeLectures){
                                print("${lectura}m ")
                            }
                            println(percentOfLectures(data, numDeLectures))
                        }
                        2 ->{
                            val numDeLectures = filterOfMountainsByMeters(data, "professional", 0, 0)
                            println("Muntanyes de 5000m a 8000m")
                            for (lectura in numDeLectures){
                                print("${lectura}m ")
                            }
                            println(percentOfLectures(data, numDeLectures))

                        }
                        3 ->{
                            val numDeLectures = filterOfMountainsByMeters(data, "federat", 0, 0)
                            println("Muntanyes de 3500m a 5000m")
                            for (lectura in numDeLectures){
                                print("${lectura}m ")
                            }
                            println(percentOfLectures(data, numDeLectures))

                        }
                        4 ->{
                            println("Entra els metres mínims i màxims")
                            val min = scanner.nextLine().toInt()
                            val max = scanner.nextLine().toInt()
                            val numDeLectures = filterOfMountainsByMeters(data, "custom", min, max)
                            println("Muntanyes de ${min}m a ${max}m")
                            for (lectura in numDeLectures){
                                print("${lectura}m ")
                            }
                            println(percentOfLectures(data, numDeLectures))
                        }

                        0 -> {
                            println("""
                                |Tornant al menu principal
                            |          ...""".trimMargin())
                        }
                    }
                    if (subInstruction !=0 ) println("""
                        |
                        |Tornant al sub menu
                            |        ...""".trimMargin())
                } while (subInstruction != 0)
            }
            0 -> println("Tancant aplicació")
        }
        if (instruction !=0 ) println("""
            |
            |Tornant al menu principal
            |          ...
        """.trimMargin())
    } while (instruction != 0)


}

fun filterOfMountainsByMeters(data: String, nivell: String, min: Int, max: Int): List<Int> {
    val registro = data.split(",")
    val listOfLecturas = mutableListOf<Int>()

    when (nivell) {
        "extrem" -> {
            for (i in 0..registro.lastIndex) {
                if (registro[i].toInt() > 8000) {
                    listOfLecturas.add(registro[i].toInt())
                }
            }
        }

        "professional" -> {
            for (i in 0..registro.lastIndex) {
                if (registro[i].toInt() in 5000..8000) {
                    listOfLecturas.add(registro[i].toInt())
                }
            }
        }

        "federat" -> {
            for (i in 0..registro.lastIndex) {
                if (registro[i].toInt() in 3500..5000) {
                    listOfLecturas.add(registro[i].toInt())
                }
            }
        }
        "custom" -> {
            for (i in 0..registro.lastIndex) {
                if (registro[i].toInt() in (min..max)) {
                    listOfLecturas.add(registro[i].toInt())
                }
            }
        }
    }
    return listOfLecturas
}





fun percentOfLectures(data: String, listOfLectures: List<Int>): String {
    val registro = data.split(",")
    var numLectures = 0
    for (i in listOfLectures) {
        numLectures++
    }
    return "\nPercentatge de lectures respecte el total ${(numLectures * 100) / registro.size}%"

}

fun alçadaMaxAndMin(data: String):String {
    val registro =  data.split(",")
    var alcadaMin = 0
    var alcadaMax = 0
    for (i in 0..registro.lastIndex){
        if (registro[i].toInt() > alcadaMax){
            alcadaMax = registro[i].toInt()
            if (i == 0){
                alcadaMin = alcadaMax
            }
        } else if (registro[i].toInt() < alcadaMin){
            alcadaMin = registro[i].toInt()
        }
    }
    return  "Alçada mínima: ${alcadaMin}m | Alçada màxima: ${alcadaMax}m "
}
fun quantitatRegistresPerSobre3500 (data:String):Int {
    val registro =  data.split(",")
    var counter = 0

    for (i in 0..registro.lastIndex) {
        if (registro[i].toInt() > 3500) {
            counter ++
        }
    }
    return counter
}
fun alçadaMitjana(data: String): Int {
    val registro = data.split(",")
    var suma = 0

    for (i in registro) {
        suma += i.toInt()
    }

    return suma / registro.size

}

fun menu(){
    println("""═══════════════════════════════════════════════════════
    | 1- Mostrar dades llegides dels cims
        | 2- Mostrar quantitat de muntanyes per sobre de 3500m
        | 3- Mostrar la mitjana d’alçades dels cims
        | 4- Mostrar alçada mínima i màxima dels cims
        | 5- Aprofitament de les dades
        | 0- Sortir de l’aplicació
        |═══════════════════════════════════════════════════════""".trimMargin())
}
fun subMenu(){
    println("""═══════════════════════════════════════════════════════
    | 1- Nivell extrem: Muntanyes de més de 8000m
| 2- Nivell professional: Muntanyes de 5000m a 8000m
| 3- Nivell federat: Muntanyes de 3500m a 5000m
| 4- Cerca customitzada: Introduir metres mínims i màxims
| 0- Tornar al menú principal
|═══════════════════════════════════════════════════════
""".trimMargin())

}

