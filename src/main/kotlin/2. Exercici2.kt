fun main(){
    println("""Si vols convertir la primera lletra de cada paraula a majúscula, escriu 1
                |Si vols convertir l'última lletra de cada paraula a minúscula, escriu 2
                |Si vols sortir, escriu 0.
        """.trimMargin())
    do{
        val instruction = scanner.nextLine().uppercase()
        if (instruction == "1"){
            println("Entra la frase")
            println(firstMayus(scanner.nextLine().lowercase()))
        }
        else if (instruction == "2"){
            println("Entra la frase")
            println(lastMinus(scanner.nextLine().uppercase()))
        }
        if (instruction != "0") println("Què vols fer ara?")

    } while (instruction != "0")
    println("Tancant aplicació...")

}

fun firstMayus(frase: String): String{
    val paraules = frase.split(" ")
    var fraseConvertida = ""
    for (paraula in paraules){
        for (lletra in 0 .. paraula.lastIndex){
            if (lletra == 0){
                fraseConvertida += paraula[lletra].uppercase()
            } else {
                fraseConvertida += paraula[lletra]
            }
        }
        fraseConvertida += " "
    }
    return fraseConvertida
}

fun lastMinus(frase: String): String{
    val paraules = frase.split(" ")
    var fraseConvertida = ""
    for (paraula in paraules){
        for (lletra in 0 .. paraula.lastIndex){
            if (lletra == paraula.lastIndex){
                fraseConvertida += paraula[lletra].lowercase()
            } else {
                fraseConvertida += paraula[lletra]
            }
        }
        fraseConvertida += " "
    }
    return fraseConvertida


}
