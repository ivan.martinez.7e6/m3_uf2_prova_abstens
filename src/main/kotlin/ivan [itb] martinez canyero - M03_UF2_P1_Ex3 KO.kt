//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"


fun main(){
    val colors = arrayOf( ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var colorPosition = 0
    var intensitat = 0
    var intensitatPujada = false
    var isOn = false

    do {
        print("Introdueix ordre: ")
        val instruccio = scanner.nextLine().uppercase()
        when(instruccio) {
            "TURN ON" -> {
                isOn =  turnOn()
                intensitat = 1
            }
            "TURN OFF" -> {
                isOn = turnOff()
                intensitat = 0
            }
            "CHANGE COLOR" -> {
                if (isOn){
                    colorPosition = changeColor(colors, colorPosition)
                } else println("No puedes cambiar el color de una lámpara apagada")
            }
            "CHANGE INTENSITY" -> {
                if (isOn){
                    val cambiarIntensitat  = changeIntensity(intensitat, intensitatPujada)
                    intensitat = cambiarIntensitat.first
                    intensitatPujada = cambiarIntensitat.second
                } else println("No puedes cambiar la intensidad de una lámpara apagada")
            }
        }
        if (instruccio != "END") {
            printInfo(isOn, colors, colorPosition, intensitat)
        }

    } while(instruccio != "END")
}

fun changeIntensity(intensitat: Int, intensitatPujada: Boolean): Pair<Int, Boolean> {
    var intensitatPujada2 = false
    var newIntensity = 0
    if (intensitat < 5 && intensitatPujada){
        newIntensity = intensitat + 1
        intensitatPujada2 = true
    }
    else if (intensitat == 5 && intensitatPujada){
        intensitatPujada2 = false
        newIntensity = intensitat - 1

    } else if (!intensitatPujada && intensitat>1){
        newIntensity = intensitat - 1
    }
    else if (!intensitatPujada && intensitat==1){
        newIntensity = intensitat + 1
        intensitatPujada2 = true
    }
    return newIntensity to intensitatPujada2
}

fun changeColor(colors: Array<String>, colorPosition: Int): Int {
    var newColorPosition = 0
    if (colorPosition < colors.size-1){
        newColorPosition = colorPosition + 1
    }
    else if(colorPosition == colors.size-1){
        newColorPosition =  1
    }
    return newColorPosition
}


fun printInfo(isOn: Boolean, colors: Array<String>, colorPosition: Int, intensitat: Int){
    if (isOn){
        println("Color: ${colors[colorPosition]}   $ANSI_RESET - intensitat $intensitat")
    } else println("Color: $ANSI_BLACK   $ANSI_RESET - intensitat $intensitat")
}


fun turnOn(): Boolean {
    return true
}
fun turnOff(): Boolean {
    return false
}



